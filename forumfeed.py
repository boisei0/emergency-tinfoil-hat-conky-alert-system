import feedparser
import re
import cPickle
import os

from naivebayes import SpamFilter

__author__ = 'boisei0'


def parse_feed(spam_filter, whitelist):
    last_known_entry = read_last_known_entry()
    data = feedparser.parse('http://crunchbang.org/forums/extern.php?action=feed&type=atom')
    for entry in data.entries:
        if entry.id == last_known_entry:
            break
        if entry.author in whitelist:
            pass
        else:
            # save_entry(entry.title, strip_html(entry.summary), entry.updated)
            if spam_filter.is_spam(u'{} {}'.format(entry.title, strip_html(entry.summary))):
                increase_spam_counter()
    save_last_known_entry(data.entries[0].id)


def strip_html(html_entry):
    html_entry = re.sub(r'<(.|\n)*?>', ' ', html_entry)
    html_entry = re.sub(r'&\w+;', ' ', html_entry)

    return html_entry


def get_filename(title, time):
    return'{}-{}.txt'.format(time, title).replace(':', '-').replace('\\', '_').replace('/', '_')


def save_last_known_entry(entry_id):
    output = open('data.pkl', 'wb')
    cPickle.dump(entry_id, output)


def read_last_known_entry():
    return cPickle.load(open('data.pkl', 'rb'))


def save_entry(title, text, time):
    with open(os.path.join('msg', get_filename(title, time)), 'w') as f:
        f.write('{}\n'.format(title))
        f.write(text)


def open_whitelist():
    return open('whitelist.txt', 'r').readlines()


def increase_spam_counter():
    spam_counter = cPickle.load(open('spam.pkl', 'rb'))
    spam_counter += 1
    cPickle.dump(spam_counter, open('spam.pkl', 'wb'))


def init_spam_counter():
    cPickle.dump(0, open('spam.pkl', 'wb'))


if __name__ == '__main__':
    sf = SpamFilter()
    wl = open_whitelist()
    parse_feed(sf, wl)
