import web
import cPickle

__author__ = 'boisei0'
__version__ = '1.0'
__conky_version__ = '2.2'

web.config.debug = False

urls = (
    '/', 'Index',
    '/spam/', 'SpamCount'
)

render = web.template.render('templates')


class Index:
    def GET(self):
        return render.index(__author__, __version__, __conky_version__)


class SpamCount:
    def GET(self):
        return self._load_spam_counter()

    def _load_spam_counter(self):
        return cPickle.load(open('spam.pkl', 'rb'))


if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()