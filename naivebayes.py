#!/usr/bin/python
# coding=utf-8
import cPickle
import os
import re
from collections import defaultdict

import nltk.classify
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from nltk import NaiveBayesClassifier

__author__ = 'boisei0'


class SpamFilter:
    def __init__(self, debug_mode=False):
        self._debug = debug_mode

        self._ham_path = '/home/boisei0/PycharmProjects/CBF Spamfilter/train-data/ham'
        self._spam_path = '/home/boisei0/PycharmProjects/CBF Spamfilter/train-data/spam'

        self._classifier = self._open_classifier()

        self._sw = stopwords.words('english')
        self._sw.extend(['ll', 've'])

    def _get_msg_dir(self, path):
        file_list = os.listdir(path)
        file_list = filter(lambda x: x != 'cmds', file_list)
        return [self._get_msg(os.path.join(path, f)) for f in file_list]

    @staticmethod
    def _get_msg(path):
        with open(path, 'rU') as f:
            msg = f.readlines()
            return ' '.join(msg)

    def _get_msg_words(self, msg):
        msg = re.sub('<(.|\n)*?>', ' ', msg)
        msg = re.sub('&\w+;', ' ', msg)
        msg = re.sub('_+', '_', msg)

        msg_words = set(wordpunct_tokenize(msg.replace('=\n', '').lower()))
        msg_words = msg_words.difference(self._sw)
        msg_words = [w for w in msg_words if re.search('[a-zA-Z]', w) and len(w) > 1]

        return msg_words

    def _features_from_messages(self, messages, label, feature_extractor, **kwargs):
        features_labels = []
        for msg in messages:
            features = feature_extractor(msg, **kwargs)
            features_labels.append((features, label))
        return features_labels

    def _word_indicator(self, msg, **kwargs):
        features = defaultdict(list)
        msg_words = self._get_msg_words(msg)
        for w in msg_words:
                features[w] = True
        return features

    def _make_train_test_sets(self, feature_extractor, **kwargs):
        train_ham_messages = self._get_msg_dir(self._ham_path)[:90]
        train_spam_messages = self._get_msg_dir(self._spam_path)[:40]

        test_ham_messages = self._get_msg_dir(self._ham_path)[90:]
        test_spam_messages = self._get_msg_dir(self._spam_path)[40:]

        train_spam = self._features_from_messages(train_spam_messages, 'spam', feature_extractor, **kwargs)
        train_ham = self._features_from_messages(train_ham_messages, 'ham', feature_extractor, **kwargs)
        train_set = train_spam + train_ham

        test_spam = self._features_from_messages(test_spam_messages, 'spam', feature_extractor, **kwargs)
        test_ham = self._features_from_messages(test_ham_messages, 'ham', feature_extractor, **kwargs)

        return train_set, test_spam, test_ham

    def _create_classifier(self, feature_extractor, **kwargs):
        train_set, test_spam, test_ham = self._make_train_test_sets(feature_extractor, **kwargs)

        classifier = NaiveBayesClassifier.train(train_set)

        if self._debug:
            print('Test Spam accuracy: {0:.2f}%'.format(100 * nltk.classify.accuracy(classifier, test_spam)))
            print('Test Ham accuracy: {0:.2f}%'.format(100 * nltk.classify.accuracy(classifier, test_ham)))

            print(classifier.show_most_informative_features(20))

        self._save_classifier(classifier)

    @staticmethod
    def _save_classifier(classifier):
        output = open('classifier.pkl', 'wb')
        cPickle.dump(classifier, output)

    def _open_classifier(self):
        if not os.path.exists('classifier.pkl'):
            self._create_classifier(self._word_indicator, stop_words=self._sw)
        else:
            return cPickle.load(open('classifier.pkl', 'rb'))

    def is_spam(self, msg):
        return nltk.classify.NaiveBayesClassifier.classify(self._classifier,
                                                           self._word_indicator(msg)) == 'spam'